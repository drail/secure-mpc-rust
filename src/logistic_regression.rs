pub mod logistic_regression {

//extern crate csv;

use crate::init::*;
use crate::protocol::*;
use crate::utility::*;
//use crate::constants::*;
use std::num::Wrapping;
use std::fs::File;
use std::time::{SystemTime};
use std::io::Write;

pub fn lr_module( ctx: &mut init::Context ) {

	let debug = ctx.debug_output;
    //(*ctx).attribute_count += 1;
	let attribute_count = ctx.attribute_count; // + 1 is a test for intercept
	let instance_count  = ctx.instance_count;
	let iterations      = ctx.iterations;

	let x_matrix = ctx.x_matrix.clone();
	let y_matrix = ctx.y_matrix.clone();

	let mut weights = vec![ Wrapping(0u64) ; attribute_count ];

	let mut i = 0;
	while i < iterations {

		if debug { 

			verify_syncing_status(ctx); 
			let weight_reveal = protocol::reveal(&weights, ctx, ctx.decimal_precision, true, false);
			println!("Weights (last 10): {:?}",  &weight_reveal[(attribute_count-10)..]);
		}

		println!("LR MODULE     : [{}] STARTING ITERATION", i);
		println!("LR MODULE     : [{}] waiting for correlated randomness", i);
      	{ // wait to be notified by ti_recvr thread
           	let &(ref lock, ref cvar) = &*ctx.wake_lr;
           	let mut cr_filled = lock.lock().unwrap();
            while !*cr_filled {		              
     			cr_filled = cvar.wait(cr_filled).unwrap();
     		}
     		*cr_filled = false;
     	}
     	println!("LR MODULE     : [{}] correlated randomness ready", i);

		/* get dot products of weigts with each instance */
		let now = SystemTime::now();
		let mut z_list = vec![Wrapping(0u64) ; instance_count ];
		for k in 0..instance_count {
			z_list[k] = protocol::dot_product( 
				&x_matrix[k], &weights, ctx, ctx.decimal_precision, true, false);
		}	
		println!("LR MODULE     : [{}] dot products complete     -- work time = {} (ms)", 
			i, now.elapsed().unwrap().as_millis() );

		if debug {
			let dp_reveal = protocol::reveal(&z_list, ctx, ctx.decimal_precision, true, false);
			println!("Dp's (last 10): {:?}",  &dp_reveal[(instance_count-10)..]);
		}

		/* get activation of each instance */
		let now = SystemTime::now();
		let o_list = batch_activate(&z_list, ctx);	
		println!("LR MODULE     : [{}] activations complete      -- work time = {} (ms)", 
			i, now.elapsed().unwrap().as_millis() );
		
		if debug {
			let o_reveal = protocol::reveal(&o_list, ctx, ctx.decimal_precision, true, false);
			println!("O's (last 10): {:?}",  &o_reveal[(instance_count-10)..]);
		}

		/* calculate sum of gradients for each instance */
		let now = SystemTime::now();
		let mut current_sum = vec![Wrapping(0u64) ; attribute_count];
		for k in 0..instance_count {
			let diff_list =  vec![y_matrix[k][0] - o_list[k] ; attribute_count];
			let gradient = protocol::batch_multiply(&x_matrix[k], &diff_list, ctx);

			for j in 0..attribute_count {
				current_sum[j] += gradient[j];
			}
		}
		for j in 0..attribute_count {
			current_sum[j] = utility::truncate_local(
				current_sum[j], ctx.decimal_precision, (*ctx).asymmetric_bit);
		}
		println!("LR MODULE     : [{}] sum of gradients complete -- work time = {} (ms)", 
			i, now.elapsed().unwrap().as_millis() );
		
		{ // notify ti_recvr
			let &(ref lock, ref cvar) = &*ctx.wake_ti;
			let mut fill_cr = lock.lock().unwrap();
			*fill_cr = true;
			cvar.notify_one();
		}

		/* update weights */
		let mut scaled_sum = vec![Wrapping(0u64) ; attribute_count];
		for k in 0..attribute_count {
			scaled_sum[k] = utility::truncate_local(
				ctx.learning_rate * current_sum[k], ctx.decimal_precision, (*ctx).asymmetric_bit);
			weights[k] += scaled_sum[k];
		}
		i += 1;
	}

	let weight_revealed = protocol::reveal(&weights, ctx, ctx.decimal_precision, true, false);
	generate_weights_file(&weight_revealed, &(*ctx).output_path);
	println!("LR MODULE     : instance completed -- weights written to:\n{}.", 
		(*ctx).output_path);
}

fn batch_activate( z_list : &Vec<Wrapping<u64>>, 
				   ctx    : &mut init::Context) -> Vec<Wrapping<u64>> {

	let frac_bitmask : u64 = (1 << (ctx.decimal_precision - 1)) - 1;
	let int_bitmask  : u64 = (1 << (ctx.integer_precision + 1)) - 1;

	let one_half: Wrapping<u64> = Wrapping(1 << (ctx.decimal_precision - 1));

	let len            = ctx.instance_count;
	let asymmetric_bit = (*ctx).asymmetric_bit as u64;
	let inversion_mask: u64 = ( - Wrapping(asymmetric_bit) ).0;

	let z_decomp_list  = protocol::batch_bit_decomp(z_list, ctx); 

	let mut msb_list      = vec![ 0u64 ; len ];
	let mut not_msb_list  = vec![ 0u64 ; len ];
	let mut z_neg_list    = vec![ Wrapping(0) ; len ];

	for i in 0..len {

		msb_list[i]     = z_decomp_list[i] >> 63;
		not_msb_list[i] = (z_decomp_list[i] >> 63) ^ asymmetric_bit;
		z_neg_list[i]   = -z_list[i];
	}

	let msb_list     = protocol::binary_vector_to_ring( &msb_list, ctx );
	let not_msb_list = protocol::binary_vector_to_ring( &not_msb_list, ctx );

	let z_unchanged = protocol::batch_multiply( &not_msb_list, &z_list, ctx );
	let z_flipped   = protocol::batch_multiply( &msb_list, &z_neg_list, ctx );
	
	let mut z_rectified = vec![ Wrapping(0u64) ; len ];

	for i in 0..len {

		z_rectified[i] = z_unchanged[i] + z_flipped[i];
	}

	let z_rectified_decomp = protocol::batch_bit_decomp(&z_rectified, ctx);

	let mut z_frac_list = vec![ 0u64 ; len ];
	let mut z_int_list  = vec![ 0u64 ; len ];

	for i in 0..len {
		z_frac_list[i] = z_rectified_decomp[i] & frac_bitmask;
		z_int_list[i]  = 
			(z_rectified_decomp[i] >> (ctx.decimal_precision - 1))  & int_bitmask;
	}

	let mut not_or_list = vec![ 0u64 ; len ]; 
	let mut or_list     = vec![ 0u64 ; len ]; 
	
	for i in 0..len {
		not_or_list[i] = (z_int_list[i] ^ inversion_mask) & 1u64;
		z_int_list[i] = (z_int_list[i] ^ inversion_mask) >> 1;
	}

	for _i in 1..ctx.integer_precision {
		not_or_list = protocol::batch_bitwise_and(&not_or_list, &z_int_list, ctx, false);

		for j in 0..len {
			z_int_list[j] >>= 1;
		}
	}

	for i in 0..len {
		not_or_list[i] &= 1u64;
		or_list[i] = (not_or_list[i] ^ inversion_mask) & 1u64;
	}

	let or_list     = protocol::binary_vector_to_ring( &or_list, ctx );
	let not_or_list = protocol::binary_vector_to_ring( &not_or_list, ctx );
	
	let z_frac_list = protocol::xor_share_to_additive( &z_frac_list, ctx, (ctx.decimal_precision-1) as usize );

	let mut const_region = vec![ Wrapping(0u64) ; len ];

	for i in 0..len {
		const_region[i] = one_half * or_list[i];
	}

	let linear_region = protocol::batch_multiply(&z_frac_list, &not_or_list, ctx);

	let mut partial_result_pos = vec![ Wrapping(0) ; len ]; 
	let mut partial_result_neg = vec![ Wrapping(0) ; len ];
	
	for i in 0..len {
		partial_result_pos[i] = const_region[i] + linear_region[i];
		partial_result_neg[i] = - partial_result_pos[i]; 
	}

	let pos_selected = protocol::batch_multiply(&partial_result_pos, &not_msb_list, ctx);
	let neg_selected = protocol::batch_multiply(&partial_result_neg, &msb_list, ctx);

	let mut activations  = vec![ Wrapping(0u64) ; len ];

	for i in 0..len {
		activations[i] = pos_selected[i] + neg_selected[i] + (Wrapping(asymmetric_bit) * one_half); // + one_half;
	}

	activations
}

fn generate_weights_file( weights: &Vec<f64>, file_path: &String ) {

	let mut file = File::create(file_path).expect("Unable to create file");
	for elem in weights.iter() {
		write!(file, "{}\n", elem).expect("Failed to output weights");
	}
}


pub fn verify_syncing_status(ctx: &mut init::Context) {

	let test_ss = vec![Wrapping(
		if (*ctx).asymmetric_bit == 1 {
			0x00000000FF000000 as u64
		} else {
			0x000000000000FF00 as u64
		}
	)];

	let test_reveal = protocol::reveal( &test_ss, ctx, ctx.decimal_precision, false, true ); 

	if 0x00000000FF00FF00 != test_reveal[0] as u64 {
		panic!("Parties not communicating correctly");
	} else {
		println!("LR MODULE     : Parties sending correct endianness");
	}

}

}

// pub fn verify_syncing_status(ctx: &mut init::Context) -> bool {

// 	let test_ss = vec![Wrapping(
// 		if (*ctx).asymmetric_bit == 1 {
// 			0x00000000AAAAAAAA as u64
// 		} else {
// 			0x0000000055555555 as u64
// 		}
// 	)];

// 	let test_reveal = protocol::reveal( &test_ss, ctx, ctx.decimal_precision, false ); 
	
// 	if 0x00000000FFFFFFFF != test_reveal[0] as u64 {
// 		println!("LR MODULE     : Parties out of sync! attempting to reset connection...");

// 		{
// 			let mut server_connected = ctx.server_connected.lock().unwrap();
// 			let mut client_connected = ctx.client_connected.lock().unwrap();

// 			*client_connected = false;
// 			*server_connected = false;
// 		}
// 		let wake_rx = &ctx.wake_rx;
// 	    { // wake main thread to read rx buf
//             let &(ref lock, ref cvar) = &**wake_rx;
// 			let mut read_ok = lock.lock().unwrap();
// 			*read_ok = true;
// 			cvar.notify_one();
//     	}

// //		thread::sleep(time::Duration::from_millis(1000));
//     	let wake_tx = &ctx.wake_tx;
// 	    { // wake main thread to read tx buf
//             let &(ref lock, ref cvar) = &**wake_tx;
// 			let mut read_ok = lock.lock().unwrap();
// 			*read_ok = true;
// 			cvar.notify_one();
//     	}

// 		loop {
// 			println!("LR MODULE     : Checking conenction status");		
// 			{
// 				let server_connected = ctx.server_connected.lock().unwrap();
// 				let client_connected = ctx.client_connected.lock().unwrap();

// 				if *server_connected && *client_connected {
// 					println!("LR MODULE     : connection status -- client={}, server={}", *client_connected, *server_connected);
// 					return false;
// 				}
// 			}	
// 			thread::sleep(time::Duration::from_millis(1000));

// 		}
// 		println!("LR MODULE     : Parties synchornized successfully; restarting iteration....");
// 	}
// 	return true;
// }

// fn load_u64_tuples( file: &mut File ) -> Vec<(u64, u64, u64)> {

//     let mut corr_rand: Vec<(u64, u64, u64)> = Vec::new();

//     let mut rdr = csv::ReaderBuilder::new()
//         .has_headers(false)
//         .from_reader(file);
//     let mut index = 0;
//     for result in rdr.deserialize() {

//         if index == 1000 {
//             break;
//         }

//         let tuple: Vec<u64> = result.unwrap();
//         corr_rand.push( (tuple[0] as u64, tuple[1] as u64, tuple[2] as u64) );
//         index += 1;
//     }
//     corr_rand
// }

// fn load_wrapping_u64_tuples( file: &mut File ) -> Vec<(Wrapping<u64>, Wrapping<u64>, Wrapping<u64>)> {

//     let mut corr_rand: Vec<(Wrapping<u64>, Wrapping<u64>, Wrapping<u64>)> = Vec::new();

//     let mut rdr = csv::ReaderBuilder::new()
//         .has_headers(false)
//         .from_reader(file);
//     let mut index = 0;
//     for result in rdr.deserialize() {

//         if index == 50000 {
//             break;
//         }

//         let tuple: Vec<u64> = result.unwrap();
//         corr_rand.push( (Wrapping(tuple[0] as u64), Wrapping(tuple[1] as u64), Wrapping(tuple[2] as u64)) );
//         index += 1;
//     }

//     corr_rand
// }


		// ctx.corr_rand = vec![
		// 	if ctx.asymmetric_bit == 1 {
		// 		constants::CR_0
		// 	} else {
		// 		constants::CR_1} 
		// 	; ctx.add_shares_per_iter ];
			
		// ctx.corr_rand_xor = vec![
		// 	if ctx.asymmetric_bit == 1 {
		// 		constants::CR_BIN_0
		// 	} else {
		// 		constants::CR_BIN_1} 
		// 	; ctx.xor_shares_per_iter ];
