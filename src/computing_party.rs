/* Party0 and Party1 connect to one another and to the 
Trusted Initializer. They train then train a secure MPC logistic
regression model on a secret-shared, distributed data set and 
output weights in the clear
*/

pub mod computing_party {

use crate::constants::*;
use crate::init::*;
//use crate::utility::*;
use crate::logistic_regression::*;
use std::net::{TcpStream, TcpListener};
use std::io::{Read, Write};
use std::thread;
use std::sync::{Arc};
use std::time::{SystemTime, Duration};
use std::num::Wrapping;

const BUF_SIZE: usize = constants::BUF_SIZE;
const TEST_U64: u64 = 0xFF00FF00FF00FF00;

pub union RxBuffer {
	buf_u64 : [ u64 ; BUF_SIZE / 8 ],
	buf_u8  : [ u8  ; BUF_SIZE ],
}

pub union SmallBuffer {
	buf_u64 : [ u64 ; 1 ],
	buf_u8  : [ u8  ; 8] ,
}

pub fn party_module( settings_file : String ) {

	println!("MAIN THREAD   : initializing runtime context and parsing inputs");
	let now = SystemTime::now();

	let mut ctx = init::initialize_runtime_context( settings_file.clone() );
	
	println!("MAIN THREAD   : input files parsed   -- work time = {} (ms)", 
		now.elapsed().unwrap().as_millis() );

    println!("MAIN THREAD   : runtime context initialized successfully --");

    let mut internal_addr; //= String::new();
    let mut external_addr; //= String::new();
    let mut ti_addr;       //= String::new();

	if ctx.asymmetric_bit == 0 {

		internal_addr = format!("{}:{}", ctx.party0_ip.clone(), ctx.party0_port);
		external_addr = format!("{}:{}", ctx.party1_ip.clone(), ctx.party1_port);
		ti_addr = format!("{}:{}", ctx.ti_ip.clone(), ctx.ti_port0);

	} else {

		internal_addr = format!("{}:{}", ctx.party1_ip.clone(), ctx.party1_port);
		external_addr = format!("{}:{}", ctx.party0_ip.clone(), ctx.party0_port);
		ti_addr = format!("{}:{}", ctx.ti_ip.clone(), ctx.ti_port1);
	}

	// Create server thread -- give copy of rx_buffer
    let rx_buffer        = Arc::clone(&ctx.rx_buffer); 
    let wake_op          = ctx.wake_op.clone();   
    let wake_rx          = ctx.wake_rx.clone();    
   // let wake_tx          = ctx.wake_tx.clone();    
    let server_connected = Arc::clone(&ctx.server_connected);
   // let client_connected = Arc::clone(&ctx.client_connected);
    let server_addr      = internal_addr.clone();

    thread::spawn(move || loop {

        let listener = TcpListener::bind( server_addr.clone() ).unwrap();
        println!("SERVER THREAD : listening on port {}", server_addr.clone() );
          
        for stream in listener.incoming() {
            match stream {
                Ok(mut stream) => {

                   println!("SERVER THREAD : New connection: {}", stream.peer_addr().unwrap() );
	                {
	               		let mut server_connected = server_connected.lock().unwrap();
	               		*server_connected = true;
	                }

					stream.set_ttl(std::u32::MAX).expect("set_ttl call failed");
					stream.set_write_timeout(None).expect("set_write_timeout call failed");
					stream.set_read_timeout(None).expect("set_read_timeout call failed");  

                	loop {

						{ // sleep until woken for rx request by main 
			               	let &(ref lock, ref cvar) = &*wake_rx;
			              	let mut receive = lock.lock().unwrap();
			                while !*receive {		              
			         			receive = cvar.wait(receive).unwrap();
			         		}
			         		*receive = false;
			         	}

			         	// read into rx buffer
	        			let mut rx_buffer = rx_buffer.lock().unwrap(); 
	        			let mut bytes_read = 0;
	        			while bytes_read < BUF_SIZE {
	        				let current_bytes = match stream.read(&mut (*rx_buffer)[bytes_read..]) {
	        					Ok(size) => size,
	        					Err(_) => panic!("SERVER : failed while writing rx buf"),
	        				};
	        				bytes_read += current_bytes;
	        			}	

			        	{ // give read access back to main thread
				            let &(ref lock, ref cvar) = &*wake_op;
							let mut read_ok = lock.lock().unwrap();
							*read_ok = true;
							cvar.notify_one();
		            	}
			   		}
			    },
                Err(e) => {
                    println!("SERVER THREAD: Error: {}", e)
                } 
        	}   
        }
    });

    // Copies of memory arc variables for client thread
    let tx_buffer        = Arc::clone(&ctx.tx_buffer);
    let wake_tx          = ctx.wake_tx.clone();
    let wake_rx			 = ctx.wake_rx.clone();
    let client_connected = Arc::clone(&ctx.client_connected);
    let extern_addr      = external_addr.clone();

    thread::spawn(move||loop {
	    match TcpStream::connect( extern_addr.clone() ) {
            Ok(mut stream) => {

 				println!("CLIENT THREAD : successfully connected to server on port {}",
               		extern_addr.clone() );

                {
               		let mut client_connected = client_connected.lock().unwrap();
               		*client_connected = true;
                }

				stream.set_ttl(std::u32::MAX).expect("set_ttl call failed");
                stream.set_write_timeout(None).expect("set_write_timeout call failed");
				stream.set_read_timeout(None).expect("set_read_timeout call failed");  
            	
            	loop {

            		{ //  sleep until woken for tx request by main 
		               	let &(ref lock, ref cvar) = &*wake_tx;
		               	let mut transmit = lock.lock().unwrap();
		                while !*transmit {		              
		         			transmit = cvar.wait(transmit).unwrap();
		         		}
		         		*transmit = false;
		         	}

					{ // notify server thread to recv rx_buffer
						let &(ref lock, ref cvar) = &*wake_rx;
						let mut receive = lock.lock().unwrap();
						*receive = true;
						cvar.notify_one();
					}

	         		{ // send tx buf
	         			let tx_buffer = tx_buffer.lock().unwrap();
	         			let mut bytes_written = 0;
	         			while bytes_written < BUF_SIZE {
							let current_bytes = match stream.write(&(*tx_buffer)[bytes_written..]) {
								Ok(size) => size,
								Err(_) => panic!("CLIENT : failed while writing tx buf"),
							};
							assert!(current_bytes > 0);
							bytes_written += current_bytes;							
	         			}
	         		}
                }
            },
            Err(e) => {
                println!("CLIENT THREAD : Failed to connect: {}", e);
            }
	    }
	});


    let ti_recvr_connected = ctx.ti_recvr_connected.clone();
    let wake_ti = ctx.wake_ti.clone();
    let wake_lr = ctx.wake_lr.clone();
    let corr_rand_add = Arc::clone(&ctx.corr_rand);
    let corr_rand_xor = Arc::clone(&ctx.corr_rand_xor);
    let add_shares_per_iter = ctx.add_shares_per_iter;
    let xor_shares_per_iter = ctx.xor_shares_per_iter;
    let rounds = ctx.iterations;

	thread::spawn(move || {
		match TcpStream::connect(ti_addr.clone()) {
			Ok(mut stream) => {

 				println!("TI RECEIVER   : successfully connected to TI on port {}",
               		ti_addr.clone() );

 				{
               		let mut ti_recvr_connected = ti_recvr_connected.lock().unwrap();
               		*ti_recvr_connected = true;          
 				}

				stream.set_ttl(std::u32::MAX).expect("set_ttl call failed");
				stream.set_write_timeout(None).expect("set_write_timeout call failed");
				stream.set_read_timeout(None).expect("set_read_timeout call failed");  

 				let triples_per_tx = (BUF_SIZE / 24) as usize;
 				let mut rx_buffer = RxBuffer { buf_u8 : [ 0u8 ; BUF_SIZE ] };
 				
            	for _round in 0..rounds {
            		println!("TI RECEIVER   : waiting to receive correlated randomness");
               		
            		{ //  sleep until woken by lr module
		               	let &(ref lock, ref cvar) = &*wake_ti;
		               	let mut get_cr = lock.lock().unwrap();
		                while !*get_cr {		              
		         			get_cr = cvar.wait(get_cr).unwrap();
		         		}
		         		*get_cr = false;
		         	}

 				    let mut confirm_buf = SmallBuffer { buf_u64 : [TEST_U64 ; 1] };
            		let mut bytes_written = 0;
            		unsafe{
        			while bytes_written < confirm_buf.buf_u8.len() {
        				let current_bytes = match stream.write(&mut (confirm_buf.buf_u8)[bytes_written..]) {
        					Ok(size) => size,
        					Err(_) => panic!("TI RECEIVER   : failed while reading init msg from P0"),
        				};
        				assert!(current_bytes != 0);
        				bytes_written += current_bytes;
        			}   
        			println!("TI RECEIVER   : sent {:X}", confirm_buf.buf_u64[0]);
        			   
        			} 


            		println!("TI RECEIVER   : receiving additive shares");
         			{
	         			let mut corr_rand_add = corr_rand_add.lock().unwrap();
	        			*corr_rand_add = Vec::new();
	        			assert_eq!((*corr_rand_add).len(), 0);
	         			unsafe {
		         			while corr_rand_add.len() < add_shares_per_iter {
		         				
			        			let mut bytes_read = 0; 			
			        			while bytes_read < BUF_SIZE {
			        				let current_bytes = match stream.read(&mut (rx_buffer.buf_u8)[bytes_read..]) {
			        					Ok(size) => size,
			        					Err(_) => panic!("TI RECVR : failed while reading additive shares"),
			        				};
									assert!(current_bytes > 0);
			        				bytes_read += current_bytes;
			        			}
			        		
				        		for i in 0..triples_per_tx {
				        			let u = Wrapping(rx_buffer.buf_u64[3*i]);
				        			let v = Wrapping(rx_buffer.buf_u64[3*i+1]);
				        			let w = Wrapping(rx_buffer.buf_u64[3*i+2]);
				        			
				        			corr_rand_add.push( (u, v, w) );
				        		}
			        		}
		        		}
		        		assert!(add_shares_per_iter <= corr_rand_add.len());
		        		//println!("ADD first-last = {:#?} {:#?}", corr_rand_add[0].0, corr_rand_add[corr_rand_add.len()-1].0);
         			}

            		println!("TI RECEIVER   : additive shares received -- receiving xor shares");
					{
	         			let mut corr_rand_xor = corr_rand_xor.lock().unwrap();
	        			*corr_rand_xor = Vec::new();
	        			assert_eq!((*corr_rand_xor).len(), 0);

	         			unsafe {
		         			while corr_rand_xor.len() < xor_shares_per_iter {
			        			
			        			let mut bytes_read = 0;
			        			while bytes_read < BUF_SIZE {
			        				let current_bytes = match stream.read(&mut (rx_buffer.buf_u8)[bytes_read..]) {
			        					Ok(size) => size,
			        					Err(_) => panic!("TI RECVR: failed while reading xor shares"),
			        				};
			        				bytes_read += current_bytes;

			        			}
			        		
				        		for i in 0..triples_per_tx {
				        			let u = rx_buffer.buf_u64[3*i];
				        			let v = rx_buffer.buf_u64[3*i+1];
				        			let w = rx_buffer.buf_u64[3*i+2];
				        			
				        			corr_rand_xor.push( (u, v, w) );
				        		}
			        		}
		        		}
		        		assert!(xor_shares_per_iter <= corr_rand_xor.len());
		        		//println!("XOR first-last = {} {}", corr_rand_xor[0].0, corr_rand_xor[corr_rand_xor.len()-1].0);
         			}
            		println!("TI RECEIVER   : xor shares received -- notifying lr thread");

         			{ // notify lr thread that cr is ready
						let &(ref lock, ref cvar) = &*wake_lr;
						let mut cr_ready = lock.lock().unwrap();
						*cr_ready = true;
						cvar.notify_one();
					}
					        		            		
                }
                println!("TI RECEIVER   : thread shutting down");
            },
            Err(e) => {
                println!("TI RECEIVER   : Failed to connect: {}", e);
            }
	    }

	});

	println!("MAIN THREAD   : checking connection status");		
	loop {
		{
			let server_connected = ctx.server_connected.lock().unwrap();
			let client_connected = ctx.client_connected.lock().unwrap();
			let ti_recvr_connected = ctx.ti_recvr_connected.lock().unwrap();

			if *server_connected && *client_connected && *ti_recvr_connected {
				println!("MAIN THREAD   : connection status -- client={}, server={}, ti_recvr={}",
					*client_connected, *server_connected, *ti_recvr_connected);
				break;
			}
		}	
		thread::sleep(Duration::from_millis(1));
	}

	// train logistic regression model
   	logistic_regression::lr_module(&mut ctx);
}

}
