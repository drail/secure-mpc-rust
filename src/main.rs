extern crate idash2019_rust;
use idash2019_rust::computing_party::*;
use idash2019_rust::trusted_initializer::*;
use std::env;
use std::time::{SystemTime};
//use std::process;

fn main() {
	
  println!("MAIN MODULE    : runtime count starting...");
  let now = SystemTime::now();
  let args: Vec<String> = env::args().collect();
  let settings_file = args[1].clone();

  //println!("{}", settings_file);

	let mut settings = config::Config::default();
    settings
        .merge(config::File::with_name( &settings_file.as_str() )).unwrap()
        .merge(config::Environment::with_prefix("APP")).unwrap();

    match settings.get_bool("ti") {
    	Ok(is_ti) => {
    		if is_ti {
    			trusted_initializer::ti_module( settings_file.clone() );
    		} else {
          computing_party::party_module( settings_file.clone() ); 		
    		}
    	},
    	Err(error) => {
    		panic!("encountered a problem while parsing settings file: {:?}", error)
    	},
    };

    println!("MAIN MODULE    : exiting");
    println!("MAIN MODULE    : runtime = {} (ms)", now.elapsed().unwrap().as_millis());

}
