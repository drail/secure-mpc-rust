
/* Generates correlated randomness for Parties to use during MPC protocols */

pub mod trusted_initializer {

extern crate rand;
use rand::Rng;
use crate::constants::*;
use crate::init::*;
//use crate::utility::*;
use std::thread;
use std::net::{TcpListener};
use std::io::{Read, Write};
use std::num::Wrapping;
use std::sync::{Arc, Mutex, Condvar};

const BUF_SIZE: usize = constants::BUF_SIZE;
const TEST_U64: u64 = 0xFF00FF00FF00FF00;

pub union TxBuffer {
	buf_u64 : [ u64 ; BUF_SIZE / 8 ],
	buf_u8  : [ u8  ; BUF_SIZE ],
}

pub union SmallBuffer {
	buf_u64 : [ u64 ; 1 ],
	buf_u8  : [ u8  ; 8] ,
}

pub fn ti_module( settings_file: String ) {

	let ctx = init::initialize_ti_context( settings_file.clone() );

	println!("TI MODULE   : runtime context initialized");

	let ti_addr0 = format!("{}:{}", ctx.ti_ip.clone(), ctx.ti_port0);
	let ti_addr1 = format!("{}:{}", ctx.ti_ip.clone(), ctx.ti_port1);

	let party0_xor_shares: Arc<Mutex<Vec<(u64, u64, u64)>>> = Arc::new(Mutex::new( Vec::new() ));
	let party1_xor_shares: Arc<Mutex<Vec<(u64, u64, u64)>>> = Arc::new(Mutex::new( Vec::new() ));
	let party0_add_shares: Arc<Mutex<Vec<(u64, u64, u64)>>> = Arc::new(Mutex::new( Vec::new() ));
	let party1_add_shares: Arc<Mutex<Vec<(u64, u64, u64)>>> = Arc::new(Mutex::new( Vec::new() ));

	let wake_tx0: Arc<(Mutex<bool>, Condvar)> = Arc::new((Mutex::new(false), Condvar::new()));
	let wake_tx1: Arc<(Mutex<bool>, Condvar)> = Arc::new((Mutex::new(false), Condvar::new()));
	let wake_rng: Arc<(Mutex<bool>, Condvar)> = Arc::new((Mutex::new(false), Condvar::new()));

	let p0_xor_list  = Arc::clone( &party0_xor_shares );
	let p0_add_list  = Arc::clone( &party0_add_shares );
	let p1_xor_list  = Arc::clone( &party1_xor_shares );
	let p1_add_list  = Arc::clone( &party1_add_shares );
	let xor_per_iter = ctx.xor_shares_per_iter;
	let add_per_iter = ctx.add_shares_per_iter;
	let rounds       = ctx.iterations;
	let wake_server0 = wake_tx0.clone();
	let wake_server1 = wake_tx1.clone();
	let wake_rng_gen = wake_rng.clone();

	// thread to generate correlated randomness
	let cr_generator = thread::spawn(move || {

		println!("TI CR GEN   : beginning");

		let mut rng = rand::thread_rng();

		println!("TI CR GEN   : [0] generating xor shares");
	{
		let mut p0_xor_list = p0_xor_list.lock().unwrap();
		let mut p1_xor_list = p1_xor_list.lock().unwrap();
		
		for _i in 0..xor_per_iter {
			// generate xor shares into the arc vectors
			let (u0, v0, w0, u1, v1, w1) = generate_xor_triple(&mut rng);
			p0_xor_list.push((u0, v0, w0));
			p1_xor_list.push((u1, v1, w1));

		} 
	} // release mutex
		println!("TI CR GEN   : [0] xor shares generated, generating additive shares");
	{	
		let mut p0_add_list = p0_add_list.lock().unwrap();
		let mut p1_add_list = p1_add_list.lock().unwrap();
		
		for _i in 0..add_per_iter {
			// generate xor shares into the arc vectors
			let (u0, v0, w0, u1, v1, w1) = generate_add_triple(&mut rng);
			p0_add_list.push((u0, v0, w0));
			p1_add_list.push((u1, v1, w1));

		} 
	} // release mutex
		
		println!("TI CR GEN   : [0] additive shares generated");
		
	{ // notify server 0
		let &(ref lock, ref cvar) = &*wake_server0;
		let mut send_ok = lock.lock().unwrap();
		*send_ok = true;
		cvar.notify_one();
	}
	{ // notify server 1
		let &(ref lock, ref cvar) = &*wake_server1;
		let mut send_ok = lock.lock().unwrap();
		*send_ok = true;
		cvar.notify_one();
	}

		// repeat for all subsequent rounds
		for round in 1..rounds {

			println!("TI CR GEN   : [{}] waiting to generate CR", round);
			for i in 0..2 { // wait until notified by both	
				
		       	let &(ref lock, ref cvar) = &*wake_rng_gen;
		       	let mut transmit = lock.lock().unwrap();
		        while !*transmit {		              
		 			transmit = cvar.wait(transmit).unwrap();
		 		}
		 		*transmit = false;
		 		println!("TI CR GEN   : [{}] received {} confirmation(s)", round, i+1);
		 	}
				println!("TI CR GEN   : [{}] generating xor shares", round);
		{
			let mut p0_xor_list = p0_xor_list.lock().unwrap();
			let mut p1_xor_list = p1_xor_list.lock().unwrap();
			
			for _i in 0..xor_per_iter {
				// generate xor shares into the arc vectors
				let (u0, v0, w0, u1, v1, w1) = generate_xor_triple(&mut rng);
				p0_xor_list.push((u0, v0, w0));
				p1_xor_list.push((u1, v1, w1));

			} 
		} // release mutex
			println!("TI CR GEN   : [{}] xor shares generated, generating additive shares", round);
		{	
			let mut p0_add_list = p0_add_list.lock().unwrap();
			let mut p1_add_list = p1_add_list.lock().unwrap();
			
			for _i in 0..add_per_iter {
				// generate xor shares into the arc vectors
				let (u0, v0, w0, u1, v1, w1) = generate_add_triple(&mut rng);
				p0_add_list.push((u0, v0, w0));
				p1_add_list.push((u1, v1, w1));

			} 
		} // release mutex
			println!("TI CR GEN   : [{}] additive shares generated", round);
			// notify sender threads
			{ // notify server 0
				let &(ref lock, ref cvar) = &*wake_server0;
				let mut send_ok = lock.lock().unwrap();
				*send_ok = true;
				cvar.notify_one();
			}
			{ // notify server 1
				let &(ref lock, ref cvar) = &*wake_server1;
				let mut send_ok = lock.lock().unwrap();
				*send_ok = true;
				cvar.notify_one();
			}
		}
	}); 	

	let server_addr0 = ti_addr0.clone();
	let p0_xor_list  = Arc::clone( &party0_xor_shares );
	let p0_add_list  = Arc::clone( &party0_add_shares );
	let xor_per_iter = ctx.xor_shares_per_iter;
	let add_per_iter = ctx.add_shares_per_iter;
	let rounds       = ctx.iterations;
	let wake_tx      = wake_tx0.clone();
	let wake_rng0	 = wake_rng.clone();

	let server_0 = thread::spawn(move || {

		let listener = TcpListener::bind( server_addr0.clone() ).unwrap();
        println!("TI SERVER 0 : listening on port {}", server_addr0.clone() );
      
        for stream in listener.incoming() {
            match stream {
                Ok(mut stream) => {

                    println!("TI SERVER 0 : New connection: {}", stream.peer_addr().unwrap() );

					stream.set_ttl(std::u32::MAX).expect("set_ttl call failed");
					stream.set_write_timeout(None).expect("set_write_timeout call failed");
					stream.set_read_timeout(None).expect("set_read_timeout call failed");  

                    let triples_per_tx = (BUF_SIZE / 24) as usize;
                    let add_tx_count = ((add_per_iter / triples_per_tx) as usize ) + if add_per_iter % triples_per_tx == 0 { 0 } else { 1 };
                	let xor_tx_count = ((xor_per_iter / triples_per_tx) as usize ) + if xor_per_iter % triples_per_tx == 0 { 0 } else { 1 };
                	
                	let mut tx_buffer = TxBuffer { buf_u64 : [ 0u64 ; BUF_SIZE / 8] };
						
                	for round in 0..rounds {
                		
                	{ // wait to be notified by rng thread
		               	let &(ref lock, ref cvar) = &*wake_tx;
		               	let mut transmit = lock.lock().unwrap();
		                while !*transmit {		              
		         			transmit = cvar.wait(transmit).unwrap();
		         		}
		         		*transmit = false;
		         	}
		         		println!("TI SERVER 0 : [{}] Ready to send -- waiting for client", round);

		         		// read a confirmation message from Party 0
                		let mut confirm_buf = SmallBuffer { buf_u64 : [0u64 ; 1] };
                		let mut bytes_read = 0;
                		unsafe{
	        			while bytes_read < confirm_buf.buf_u8.len() {
	        				let current_bytes = match stream.read(&mut (confirm_buf.buf_u8)[bytes_read..]) {
	        					Ok(size) => size,
	        					Err(_) => panic!("TI SERVER 0: failed while reading init msg from P0"),
	        				};
	        				assert!(current_bytes != 0);
	        				bytes_read += current_bytes;
	        			}    

	        			println!("TI SERVER 0 : got {:X}", confirm_buf.buf_u64[0]);
	        			assert_eq!(confirm_buf.buf_u64[0], TEST_U64);   
	        			}

	        			println!("TI SERVER 0 : [{}] sending additive shares", round);
	        		{
	        			let mut add_list = p0_add_list.lock().unwrap();

	        			for _i in 0..add_tx_count {
	        				for j in 0..triples_per_tx {

	        					if add_list.len() == 0 { break; }
	        					let (u, v, w) = add_list.pop().unwrap();
	        					
	        					unsafe {
		        					tx_buffer.buf_u64[3*j]     = u;
		        					tx_buffer.buf_u64[3*j + 1] = v;
		        					tx_buffer.buf_u64[3*j + 2] = w;
		        				}	
	        				}
	        				let mut bytes_written = 0;
		         			while bytes_written < BUF_SIZE {
		         				unsafe {
									let current_bytes = match stream.write(&(tx_buffer.buf_u8[bytes_written..])) {
										Ok(size) => size,
										Err(_) => panic!("TI SERVER 0 : failed while writing additive shares"),
									};
									assert!(current_bytes != 0);
										
									bytes_written += current_bytes;
								}
							}						
	        			}
	        		}
	        			println!("TI SERVER 0 : [{}] additive shares sent, sending xor shares", round);
	        		{
	        			let mut xor_list = p0_xor_list.lock().unwrap();
	        			for _i in 0..xor_tx_count {
	        				for j in 0..triples_per_tx {

	        					if xor_list.len() == 0 { break; }
	        					let (u, v, w) = xor_list.pop().unwrap();
	        					
	        					unsafe {
		        					tx_buffer.buf_u64[3*j]     = u;
		        					tx_buffer.buf_u64[3*j + 1] = v;
		        					tx_buffer.buf_u64[3*j + 2] = w;
	        					}	        					
	        				}
	        				let mut bytes_written = 0;
		         			while bytes_written < BUF_SIZE {
								unsafe {
									let current_bytes = match stream.write(&(tx_buffer.buf_u8[bytes_written..])) {
										Ok(size) => size,
										Err(_) => panic!("TI SERVER 0 : failed while writing xor shares"),
									};
									assert!(current_bytes != 0);	
									bytes_written += current_bytes;
								}							
		         			}
	        			}
	        		}
	        			println!("TI SERVER 0 : [{}] xor shares sent", round);
						// notify rng thread

					{ // notify rng thread to generate more cr
						let &(ref lock, ref cvar) = &*wake_rng0;
						let mut generate = lock.lock().unwrap();
						*generate = true;
						cvar.notify_one();
					}

                	}
			    },
                Err(e) => {
                    println!("TI SERVER 0 : Error: {}", e)
                } 
        	}
        	break;   
        }
        println!("TI SERVER 0 : thread shutting down");

	});

	// duplicate of sever_0 TODO spawn both threads iteratively instead of
	// duplicating code
	let server_addr1 = ti_addr1.clone();
	let p1_xor_list  = Arc::clone( &party1_xor_shares );
	let p1_add_list  = Arc::clone( &party1_add_shares );
	let xor_per_iter = ctx.xor_shares_per_iter;
	let add_per_iter = ctx.add_shares_per_iter;
	let rounds       = ctx.iterations;
	let wake_tx      = wake_tx1.clone();
	let wake_rng1	 = wake_rng.clone();

	let server_1 = thread::spawn(move || {

		let listener = TcpListener::bind( server_addr1.clone() ).unwrap();
        println!("TI SERVER 1 : listening on port {}", server_addr1.clone() );
      
        for stream in listener.incoming() {
            match stream {
                Ok(mut stream) => {

                    println!("TI SERVER 1 : New connection: {}", stream.peer_addr().unwrap() );

					stream.set_ttl(std::u32::MAX).expect("set_ttl call failed");
					stream.set_write_timeout(None).expect("set_write_timeout call failed");
					stream.set_read_timeout(None).expect("set_read_timeout call failed");  

                    let triples_per_tx = (BUF_SIZE / 24) as usize;
                    let add_tx_count = ((add_per_iter / triples_per_tx) as usize ) + if add_per_iter % triples_per_tx == 0 { 0 } else { 1 };
                	let xor_tx_count = ((xor_per_iter / triples_per_tx) as usize ) + if xor_per_iter % triples_per_tx == 0 { 0 } else { 1 };
                	
                	let mut tx_buffer = TxBuffer { buf_u64 : [ 0u64 ; BUF_SIZE / 8] };
						
                	for round in 0..rounds {
                		
                	{ // wait to be notified by rng thread
		               	let &(ref lock, ref cvar) = &*wake_tx;
		               	let mut transmit = lock.lock().unwrap();
		                while !*transmit {		              
		         			transmit = cvar.wait(transmit).unwrap();
		         		}
		         		*transmit = false;
		         	}
		         		println!("TI SERVER 1 : [{}] Ready to send -- waiting for client", round);

                		let mut confirm_buf = SmallBuffer { buf_u64 : [0u64 ; 1] };
                		let mut bytes_read = 0;
                		unsafe{
	        			while bytes_read < confirm_buf.buf_u8.len() {
	        				let current_bytes = match stream.read(&mut (confirm_buf.buf_u8)[bytes_read..]) {
	        					Ok(size) => size,
	        					Err(_) => panic!("TI SERVER 0: failed while reading init msg from P0"),
	        				};
	        				assert!(current_bytes != 0);
	        				bytes_read += current_bytes;
	        			}    

	        			println!("TI SERVER 0 : got {:X}", confirm_buf.buf_u64[0]);
	        			assert_eq!(confirm_buf.buf_u64[0], TEST_U64);   
	        			}

	        			println!("TI SERVER 1 : [{}] sending additive shares", round);
	        		{
	        			let mut add_list = p1_add_list.lock().unwrap();

	        			for _i in 0..add_tx_count {
	        				for j in 0..triples_per_tx {

	        					if add_list.len() == 0 { break; }
	        					let (u, v, w) = add_list.pop().unwrap();
	        					
	        					unsafe {
		        					tx_buffer.buf_u64[3*j]     = u;
		        					tx_buffer.buf_u64[3*j + 1] = v;
		        					tx_buffer.buf_u64[3*j + 2] = w;
		        				}	
	        				}
	        				let mut bytes_written = 0;
		         			while bytes_written < BUF_SIZE {

		         				unsafe {

									let current_bytes = match stream.write(&(tx_buffer.buf_u8[bytes_written..])) {
										Ok(size) => size,
										Err(_) => panic!("TI SERVER 1 : failed while writing additive shares"),
									};	
									assert!(current_bytes != 0);
									bytes_written += current_bytes;
									
								}							
		         			}
	        			}
	        		}
	        			println!("TI SERVER 1 : [{}] additive shares sent, sending xor shares", round);
	        		{
	        			let mut xor_list = p1_xor_list.lock().unwrap();
	        			for _i in 0..xor_tx_count {
	        				for j in 0..triples_per_tx {

	        					if xor_list.len() == 0 { break; }
	        					let (u, v, w) = xor_list.pop().unwrap();
	        					
	        					unsafe {

		        					tx_buffer.buf_u64[3*j]     = u;
		        					tx_buffer.buf_u64[3*j + 1] = v;
		        					tx_buffer.buf_u64[3*j + 2] = w;
	        					}	        					
	        				}
	        				let mut bytes_written = 0;
		         			while bytes_written < BUF_SIZE {
								unsafe {
									let current_bytes = match stream.write(&(tx_buffer.buf_u8[bytes_written..])) {
										Ok(size) => size,
										Err(_) => panic!("TI SERVER 1 : failed while writing xor shares"),
									};	
									assert!(current_bytes != 0);
									bytes_written += current_bytes;
								}							
		         			}
	        			}
	        		}
	        			println!("TI SERVER 1 : [{}] xor shares sent", round);

					{ // notify rg thread to generate more cr
						let &(ref lock, ref cvar) = &*wake_rng1;
						let mut generate = lock.lock().unwrap();
						*generate = true;
						cvar.notify_one();
					}

                	}
			    },
                Err(e) => {
                    println!("TI SERVER 1 : Error: {}", e)
                } 
        	}
        	break;   
        }
        println!("TI SERVER 1 : thread shutting down");

	});


	cr_generator.join().expect("Failed to join CR generator");	
	server_1.join().expect("Failed to join SERVER 1");
	server_0.join().expect("Failed to join SERVER 0");

	println!("TI MODULE   : exiting");

}

/* generate group of 64 Beaver triples over Z_2 */
fn generate_xor_triple(rng : &mut rand::ThreadRng) -> (u64, u64, u64, u64, u64, u64) {

	let u: u64  = rng.gen();
	let v: u64  = rng.gen();
	let w  		= u & v;
	let u0: u64 = rng.gen();
	let v0: u64 = rng.gen();
	let w0: u64 = rng.gen();
	let u1 		= u ^ u0;
	let v1 		= v ^ v0;
	let w1 		= w ^ w0;

	(u0, v0, w0, u1, v1, w1)
}

/* generate Beaver triples over Z_2^64 */
fn generate_add_triple(rng : &mut rand::ThreadRng) -> (u64, u64, u64, u64, u64, u64) {

	let u: u64  = rng.gen();
	let v: u64  = rng.gen();
	let w  		= (Wrapping(u) * Wrapping(v)).0;
	let u0: u64 = rng.gen();
	let v0: u64 = rng.gen();
	let w0: u64 = rng.gen();
	let u1 		= (Wrapping(u) - Wrapping(u0)).0;
	let v1 		= (Wrapping(v) - Wrapping(v0)).0;
	let w1 		= (Wrapping(w) - Wrapping(w0)).0;

	(u0, v0, w0, u1, v1, w1)
}

}
