/*
 runtime context structures for Parties and Trusted Initializer
 */

pub mod init {

extern crate csv;
extern crate config;
use crate::constants::*;
use std::sync::{Arc, Mutex, Condvar};
use std::num::Wrapping;
use std::fs::File;

const BUF_SIZE: usize = constants::BUF_SIZE;

pub struct Context {

    /* options */
    pub debug_output: bool,

    /* network */
	pub party_id: u8,
    pub ti_ip: String,
	pub ti_port0: u16,
    pub ti_port1: u16,
    pub party0_ip: String,
    pub party0_port: u16,
    pub party1_ip: String,
    pub party1_port: u16,
    
    /* mpc */
    pub asymmetric_bit: u8,
    pub corr_rand: Arc<Mutex<Vec<(Wrapping<u64>, Wrapping<u64>, Wrapping<u64>)>>>,
    pub corr_rand_xor: Arc<Mutex<Vec<(u64, u64, u64)>>>,   
    pub xor_shares_per_iter: usize,
    pub add_shares_per_iter: usize,

    /* logistic regression */
	pub x_input_path: String,
	pub y_input_path: String,
	pub output_path: String,
    pub x_matrix: Vec<Vec<Wrapping<u64>>>,
    pub y_matrix: Vec<Vec<Wrapping<u64>>>,
	pub attribute_count: usize,
	pub instance_count: usize,
	pub iterations: u32,
    pub learning_rate_float: f64,
    pub learning_rate: Wrapping<u64>,
    pub decimal_precision: u32,
    pub integer_precision: u32,

    /* system */
    pub tx_buffer: Arc<Mutex<[u8 ; BUF_SIZE]>>,
	pub rx_buffer: Arc<Mutex<[u8 ; BUF_SIZE]>>,
	pub wake_tx: Arc<(Mutex<bool>, Condvar)>,
	pub wake_rx: Arc<(Mutex<bool>, Condvar)>,
	pub wake_op: Arc<(Mutex<bool>, Condvar)>,
    pub wake_lr: Arc<(Mutex<bool>, Condvar)>,
    pub wake_ti: Arc<(Mutex<bool>, Condvar)>,
	pub client_connected: Arc<Mutex<bool>>,
	pub server_connected: Arc<Mutex<bool>>,
    pub ti_recvr_connected: Arc<Mutex<bool>>,
}

pub struct TiContext {

    pub ti_ip: String,
    pub ti_port0: u16,
    pub ti_port1: u16,
    pub iterations: u32,
    pub xor_shares_per_iter: usize,
    pub add_shares_per_iter: usize,
}

pub fn initialize_ti_context( settings_file : String ) -> TiContext {

    let mut settings = config::Config::default();
    settings
        .merge(config::File::with_name( settings_file.as_str() )).unwrap()
        .merge(config::Environment::with_prefix("APP")).unwrap();

    let ti_ip = match settings.get_str("ti_ip") {
        Ok(num) => num as String,
        Err(error) => {
            panic!("Encountered a problem while parsing ti_ip: {:?} ", error)
        },
    };

    let ti_port0 = match settings.get_int("ti_port0") {
        Ok(num) => num as u16,
        Err(error) => {
            panic!("Encountered a problem while parsing ti_port0: {:?} ", error)
        },
    };

    let ti_port1 = match settings.get_int("ti_port1") {
        Ok(num) => num as u16,
        Err(error) => {
            panic!("Encountered a problem while parsing ti_port1: {:?} ", error)
        },
    };

    let iterations = match settings.get_int("iterations") {
        Ok(num) => num as u32,
        Err(error) => { 
            panic!("Encountered a problem while parsing iterations: {:?}", error) 
        },
    };

    let xor_shares_per_iter = match settings.get_int("xor_shares_per_iter") {
        Ok(num) => num as usize,
        Err(error) => { 
            panic!("Encountered a problem while parsing xor_shares_per_iter: {:?}", error) 
        },
    };

    let add_shares_per_iter = match settings.get_int("add_shares_per_iter") {
        Ok(num) => num as usize,
        Err(error) => { 
            panic!("Encountered a problem while parsing add_shares_per_iter: {:?}", error) 
        },
    };     

    TiContext {

        ti_ip    : ti_ip,
        ti_port0 : ti_port0,
        ti_port1 : ti_port1,
        iterations : iterations,
        xor_shares_per_iter : xor_shares_per_iter,
        add_shares_per_iter : add_shares_per_iter,
    }
}

pub fn initialize_runtime_context( settings_file: String ) -> Context {

	let mut settings = config::Config::default();
    settings
        .merge(config::File::with_name( &settings_file.as_str() )).unwrap()
        .merge(config::Environment::with_prefix("APP")).unwrap();


    let debug_output = match settings.get_bool("debug_output") {
        Ok(num) => num as bool,
        Err(error) => { 
            panic!("Encountered a problem while parsing debug_output: {:?}", error) 
        },
    };

    let party_id = match settings.get_int("party_id") {
    	Ok(num) => num as u8,
    	Err(error) => { 
    		panic!("Encountered a problem while parsing party_id: {:?}", error) 
    	},
    };

    let ti_ip = match settings.get_str("ti_ip") {
        Ok(num) => num as String,
        Err(error) => {
            panic!("Encountered a problem while parsing ti_ip: {:?} ", error)
        },
    };


    let ti_port0 = match settings.get_int("ti_port0") {
    	Ok(num) => num as u16,
    	Err(error) => {
    		panic!("Encountered a problem while parsing ti_port0: {:?} ", error)
    	},
    };


    let ti_port1 = match settings.get_int("ti_port1") {
        Ok(num) => num as u16,
        Err(error) => {
            panic!("Encountered a problem while parsing ti_port1: {:?} ", error)
        },
    };

    let party0_ip = match settings.get_str("party0_ip") {
        Ok(num) => num as String,
        Err(error) => {
            panic!("Encountered a problem while parsing party0_ip: {:?} ", error)
        },
    }; 

    let party1_ip = match settings.get_str("party1_ip") {
        Ok(num) => num as String,
        Err(error) => {
            panic!("Encountered a problem while parsing party1_ip: {:?} ", error)
        },
    }; 

    let party0_port = match settings.get_int("party0_port") {
    	Ok(num) => num as u16,
    	Err(error) => {
    		panic!("Encountered a problem while parsing party0_port: {:?} ", error)
    	},
    };
    
    let party1_port = match settings.get_int("party1_port") {
    	Ok(num) => num as u16,
    	Err(error) => {
    		panic!("Encountered a problem while parsing party1_port: {:?} ", error)
    	},
    };

    let x_input_path = match settings.get_str("x_input_path") {
    	Ok(string) => string,
    	Err(error) => {
    		panic!("Encountered a problem while parsing x_input_path: {:?}", error)
    	},
    };

   let y_input_path = match settings.get_str("y_input_path") {
    	Ok(string) => string,
    	Err(error) => {
    		panic!("Encountered a problem while parsing y_input_path: {:?}", error)
    	},
    };
 
    let output_path = match settings.get_str("output_path") {
    	Ok(string) => string,
    	Err(error) => {
    		panic!("Encountered a problem while parsring weights_output_path: {:?}", error)
    	},
    };


    let iterations = match settings.get_int("iterations") {
        Ok(num) => num as u32,
        Err(error) => { 
            panic!("Encountered a problem while parsing iterations: {:?}", error) 
        },
    };

    let learning_rate_float = match settings.get_float("learning_rate") {
        Ok(num) => num as f64,
        Err(error) => { 
            panic!("Encountered a problem while parsing learning_rate: {:?}", error) 
        },
    };

    let attribute_count = match settings.get_int("attribute_count") {
        Ok(num) => num as usize,
        Err(error) => { 
            panic!("Encountered a problem while parsing attribute_count: {:?}", error) 
        },
    };

    let instance_count = match settings.get_int("instance_count") {
        Ok(num) => num as usize,
        Err(error) => { 
            panic!("Encountered a problem while parsing instance: {:?}", error) 
        },
    };    

    let decimal_precision = match settings.get_int("decimal_precision") {
        Ok(num) => num as u32,
        Err(error) => { 
            panic!("Encountered a problem while parsing instance: {:?}", error) 
        },
    };    

    let integer_precision = match settings.get_int("integer_precision") {
        Ok(num) => num as u32,
        Err(error) => { 
            panic!("Encountered a problem while parsing instance: {:?}", error) 
        },
    };    

    let xor_shares_per_iter = match settings.get_int("xor_shares_per_iter") {
        Ok(num) => num as usize,
        Err(error) => { 
            panic!("Encountered a problem while parsing xor_shares_per_iter: {:?}", error) 
        },
    };

    let add_shares_per_iter = match settings.get_int("add_shares_per_iter") {
        Ok(num) => num as usize,
        Err(error) => { 
            panic!("Encountered a problem while parsing add_shares_per_iter: {:?}", error) 
        },
    };

   let learning_rate = Wrapping( (learning_rate_float * ((2u64.pow(decimal_precision) as f64))) as u64 );


	Context {

        debug_output        : debug_output,
		party_id 		    : party_id,
        ti_ip               : ti_ip,
		ti_port0		    : ti_port0,
        ti_port1            : ti_port1,
        party0_ip           : party0_ip,
        party0_port         : party0_port,
        party1_ip           : party1_ip,
		party1_port         : party1_port,
        asymmetric_bit      : party_id,
        x_matrix            : load_u64_matrix(&x_input_path, instance_count as usize, false,  (party_id as u64) << decimal_precision),
        y_matrix            : load_u64_matrix(&y_input_path, instance_count as usize, false, (party_id as u64) << decimal_precision),
        x_input_path        : x_input_path,
        y_input_path        : y_input_path,
        output_path         : output_path,
        attribute_count     : attribute_count,
		instance_count      : instance_count,
        iterations          : iterations,
        add_shares_per_iter : add_shares_per_iter,
        xor_shares_per_iter : xor_shares_per_iter,
        learning_rate_float : learning_rate_float,
        learning_rate       : learning_rate,
        decimal_precision   : decimal_precision,
        integer_precision   : integer_precision,
		corr_rand           : Arc::new(Mutex::new(Vec::new())),
        corr_rand_xor       : Arc::new(Mutex::new(Vec::new())), /* */
		rx_buffer           : Arc::new(Mutex::new( [0x00 ; BUF_SIZE] )),
		tx_buffer           : Arc::new(Mutex::new( [0x00 ; BUF_SIZE] )),
		wake_tx             : Arc::new((Mutex::new(false), Condvar::new())),
		wake_rx             : Arc::new((Mutex::new(false), Condvar::new())),
		wake_op             : Arc::new((Mutex::new(false), Condvar::new())),
        wake_lr             : Arc::new((Mutex::new(false), Condvar::new())),
        wake_ti             : Arc::new((Mutex::new(true), Condvar::new())),
		server_connected    : Arc::new(Mutex::new(false)),
		client_connected    : Arc::new(Mutex::new(false)),
        ti_recvr_connected  : Arc::new(Mutex::new(false)),
	}

}

fn load_u64_matrix( file_path: &String, instances: usize, add_dummy: bool, one: u64 ) -> Vec<Vec<Wrapping<u64>>> {

    let mut matrix: Vec<Vec<Wrapping<u64>>> = vec![ Vec::new() ; instances ];

    let file = File::open(file_path);
    let mut rdr = csv::ReaderBuilder::new()
        .has_headers(false)
        .from_reader(file.unwrap());

    let mut index = 0;
    for result in rdr.deserialize() {

        if index == instances {
            break;
        }

        matrix[index] = result.unwrap();
        if add_dummy {
            matrix[index].push(Wrapping(one)); // TODO: Ensure this is enough
        }
        index += 1;
    }
    matrix
}

}