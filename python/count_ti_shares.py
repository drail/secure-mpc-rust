
import sys
from math import ceil

def batch_mult(n):

    return n, 0

def dp(n):

    return batch_mult(n)

def bin_to_ring(n):

    return batch_mult(n)

def xor_to_additive(n, size):

    add1, xor1 = bin_to_ring(size) # n times
    return add1 * n, xor1 * n

def batch_and(n):

    return 0, n

def batch_bd(n):

    add1, xor1 = batch_and(n)
    add2, xor2 = batch_and( ceil(float(n)/64) )

    return add1 + 2 * 63 * add2, xor1 + 2 * 63 * xor2

def activate(n, int_acc, dec_acc):

    add1, xor1 = batch_bd(n) # 2 verified
    add2, xor2 = bin_to_ring(n) # 4 verified
    add3, xor3 = batch_mult(n) # 5 verified
    add4, xor4 = batch_and(n) # int_acc - 1 verified
    add5, xor5 = xor_to_additive(n, dec_acc - 1)

    add = 2 * add1 + 4 * add2 + 5 * add3 + (int_acc-1) * add4 + add5
    xor = 2 * xor1 + 4 * xor2 + 5 * xor3 + (int_acc-1) * xor4 + xor5

    return add, xor

def lr(n, attr_cnt, iters, dec_acc, int_acc):

    add1, xor1 = dp(attr_cnt) # n times per iter
    add2, xor2 = activate(n, int_acc, dec_acc) # once per iter
    add3, xor3 = batch_mult(attr_cnt) # n times per iter

    add = iters * (n * add1 + add2 + n * add3)
    xor = iters * (n * xor1 + xor2 + n * xor3)

    return add, xor
        
if __name__=='__main__':

    decimal_precision = 10
    integer_precision = 15 

    # GSE 2034
    attribute_count = 12634
    instance_count = 179
    iterations = 223

    # BC-TCGA           
    # attribute_count = 17814 
    # instance_count = 375    
    # iterations = 10

    add, xor = lr(instance_count,\
                  attribute_count + 1,\
                  iterations,\
                  decimal_precision,\
                  integer_precision)

    print('total additive: ', add)
    print('total xor     : ', xor)
    print('add per iter: ', add // iterations)
    print('xor per iter: ', xor // iterations)
    
